package note.utils;

/**
 * Created by iuliana on 4/3/2018.
 */
public class ClasaException extends Exception {
    public ClasaException(String emptyRepository) {
        super(emptyRepository);
    }
}
