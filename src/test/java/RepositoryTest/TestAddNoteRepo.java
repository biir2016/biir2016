package RepositoryTest;

import note.controller.NoteController;
import note.model.Nota;
import note.utils.ClasaException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static note.utils.Constants.invalidMateria;
import static note.utils.Constants.invalidNota;
import static note.utils.Constants.invalidNrmatricol;
import static org.junit.Assert.assertEquals;

/**
 * Created by iuliana on 4/22/2018.
 */
public class TestAddNoteRepo {
    private NoteController notaCtrl;

    @Before
    public void init(){
        notaCtrl = new NoteController();
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testSize() throws Exception {
        //Test 1

        assertEquals(0, notaCtrl.getNote().size());

        Nota nota1 = new Nota(1, "Fizica", 8.5);
        Nota nota2 = new Nota(999,"Engleza", 10);
        Nota nota3 = new Nota(2,"Franceza", 4);
        try {
            notaCtrl.addNota(nota1);
            notaCtrl.addNota(nota2);
            notaCtrl.addNota(nota3);
        } catch (ClasaException e) {
            throw e;
        }

        assertEquals(3, notaCtrl.getNote().size());
        assertEquals(notaCtrl.getNote().get(2).getNrmatricol(), 2);
        assertEquals(notaCtrl.getNote().get(2).getMaterie(), "Franceza");
        assertEquals(notaCtrl.getNote().get(2).getNota(), 4, 0.001);
    }

    @Test
    public void testCorrectNrMat() throws ClasaException {
        //Test 2
        //numar matricol care este in intervalul 1..1000
        Nota nota = new Nota(10, "Engleza", 5);
        notaCtrl.addNota(nota);
    }

    @Test
    public void tesIncorrectNrMat1() throws ClasaException {
        //Test 3
        //numar matricol care este inafara intervalul 1..1000/ limita din inferioara
        expectedException.expect(ClasaException.class);
        expectedException.expectMessage(invalidNrmatricol);
        Nota nota = new Nota(-3, "Geografie", 10);
        notaCtrl.addNota(nota);
    }

    @Test
    public void tesIncorrectNrMat2() throws ClasaException {
        //Test 4
        //numar matricol care este inafara intervalul 1..1000/ limita din superioara
        expectedException.expect(ClasaException.class);
        expectedException.expectMessage(invalidNrmatricol);
        Nota nota = new Nota(1001, "Geografie", 10);
        notaCtrl.addNota(nota);
    }

    @Test
    public void testCorrectNota1() throws ClasaException {
        //Test 5
        //nota cuprinsa intre 1 si 10 (numar real)
        Nota nota = new Nota(10, "Istorie", 5.5);
        notaCtrl.addNota(nota);
    }

    @Test
    public void testCorrectNota2() throws ClasaException {
        //Test 6
        //nota cuprinsa intre 1 si 10
        Nota nota = new Nota(10, "Istorie", 10);
        notaCtrl.addNota(nota);
    }

    @Test
    public void testIncorrectNota1() throws ClasaException {
        //Test 7
        //nota inafara intervalului 1..10 (limita inferioara)
        expectedException.expect(ClasaException.class);
        expectedException.expectMessage(invalidNota);
        Nota nota = new Nota(88, "Latina", 0.1);
        notaCtrl.addNota(nota);
    }

    @Test
    public void testIncorrectNota2() throws ClasaException {
        //Test 8
        //nota inafara intervalului 1..10 (limita superioara)
        expectedException.expect(ClasaException.class);
        expectedException.expectMessage(invalidNota);
        Nota nota = new Nota(68, "Romana", 11);
        notaCtrl.addNota(nota);
    }

    @Test
    public void testCorrectMateria() throws ClasaException {
        //Test 9
        //lungime materie intre 5..20 de caractere
        Nota nota = new Nota(10, "Chimie", 5.5);
        notaCtrl.addNota(nota);
    }

    @Test
    public void testIncorrectMateria1() throws ClasaException {
        //Test 10
        //lungime materie inafara intervalului 5..20 de caractere (limita inferioara)
        expectedException.expect(ClasaException.class);
        expectedException.expectMessage(invalidMateria);
        Nota nota = new Nota(12, "Info", 1);
        notaCtrl.addNota(nota);
    }

    @Test
    public void testIncorrectMateria2() throws ClasaException {
        //Test 11
        //lungime materie inafara intervalului 5..20 de caractere (limita superioara)
        expectedException.expect(ClasaException.class);
        expectedException.expectMessage(invalidMateria);
        Nota nota = new Nota(68, "RomanaRomanaRomanaRomanaRomana", 10);
        notaCtrl.addNota(nota);
    }

}
